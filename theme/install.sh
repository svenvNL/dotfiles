#!/bin/bash

git clone https://github.com/heychrisd/Boston-Icons.git ~/.icons/Boston-Icons
gsettings set org.gnome.desktop.interface icon-theme "Boston-Icons"
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:appmenu'

git clone https://github.com/JetBrains/JetBrainsMono.git ~/Projects/github/JetBrains/JetBrainsMono

mkdir ~/.fonts

for font in ~/Projects/github/JetBrains/JetBrainsMono/ttf/*.ttf
do ln -s "${font}" "${HOME}/.fonts/$(basename ${font})";
done

fc-cache
