#!/bin/sh

. ./lib/os.sh
get_os

if [ "${OS}" == "fedora" ]; then
    sh ./os/fedora/pre-install.sh
    sh ./os/fedora/cleanup.sh
fi

sh ./software/alacritty/install.sh
sh ./software/chrome/install.sh
sh ./software/docker/install.sh
sh ./software/git/install.sh
sh ./software/gparted/install.sh
sh ./software/gradle/install.sh
sh ./software/java/install.sh
sh ./software/liferea/install.sh
sh ./software/nodejs/install.sh
sh ./software/pcmanfm/install.sh
sh ./software/postman/install.sh
sh ./software/shotwell/install.sh
sh ./software/spotify/install.sh
sh ./software/thunderbird/install.sh
sh ./software/vlc/install.sh
sh ./software/vscode/install.sh
sh ./software/yarn/install.sh
sh ./software/zsh/install.sh

sh ./theme/install.sh

if [ "${OS}" == "fedora" ]; then
    sh ./os/fedora/post-install.sh
fi
