function get_os() {
    release="$(cat /etc/os-release | grep -w ID)"

    IFS='='
    read -ra ADDR <<< "$release"

    OS=${ADDR[1]}
    IFS=' '
}
