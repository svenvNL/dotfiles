#!/bin/sh

sudo dnf remove -y \
cheese \
cockpit \
evolution \
gnome-boxes \
gnome-calculator \
gnome-calendar \
gnome-clocks \
gnome-contacts \
gnome-documents \
gnome-logs \
gnome-maps \
gnome-photos \
gnome-weather \
nautilus \
rhythmbox \
simple-scan \
totem \
