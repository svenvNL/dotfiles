# Sven's Dotfiles

These are my Linux dotfiles. Feel free to make suggestions or send a pull request, but keep in mind these dotfiles are mine. You can always fork this repo and make edits to to fulfil your needs.

## Installation

!WARNING: These scripts will override your config files. Make shure to make a backup before running the following script!

These scripts should be Linux agnostic exlcuding the `setupSoftware` function in [setup.sh](./setup.sh). `setupSoftware` is written for [Fedora Workstation](http://getfedora.org).

To get started, run the following command:
```bash
bash <(curl -s "https://gitlab.com/svenvNL/dotfiles/raw/master/bootstrap.sh?$(date +%s)")
```

## License

All files in this repository are licensed under the [MIT license](./LICENSE).

## Thanks to

- [Mathias Bynens](https://github.com/mathiasbynens) for sharing his [dotfiles repository](https://github.com/mathiasbynens/dotfiles)
- [Matt Gaunt](https://github.com/gauntface) for sharing his [dotfiles repository](https://github.com/gauntface/dotfiles)
