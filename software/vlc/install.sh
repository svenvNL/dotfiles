#!/bin/sh

. ./lib/os.sh
. ./lib/script.sh
get_os
get_workdir

echo -e "\n[VLC] Installing"

if ! [ -f "$WORKDIR/install-${OS}.sh" ]; then
    echo "[VLC] Unable to install";
    exit 1;
else
    . "$WORKDIR/install-${OS}.sh"
fi
