#!/bin/sh

. ./lib/script.sh
get_workdir

echo -e "\n[Postman] Installing"

if ! [ -x "$(command -v flatpak)" ]; then
    echo "[Postman] Unable to install, Flatpak is not installed";
    exit 1;
else
    . "$WORKDIR/install-flatpak.sh"
fi
