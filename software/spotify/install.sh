#!/bin/sh

. ./lib/script.sh
get_workdir

echo -e "\n[Spotify] Installing"

if ! [ -x "$(command -v flatpak)" ]; then
    echo "[Spotify] Unable to install, Flatpak is not installed";
    exit 1;
else
    . "$WORKDIR/install-flatpak.sh"
fi
