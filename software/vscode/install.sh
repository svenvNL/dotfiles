#!/bin/sh

. ./lib/os.sh
. ./lib/script.sh
get_os
get_workdir

echo -e "\n[VSCode] Installing"

if ! [ -f "$WORKDIR/install-${OS}.sh" ]; then
    echo "[VSCode] Unable to install";
    exit 1;
else
    . "$WORKDIR/install-${OS}.sh"
fi

ln -fs "$WORKDIR/config/settings.json" ~/.config/Code/User/settings.json;

extensions[0]="AndrsDC.base16-themes"
extensions[1]="bagonaut.mongogo"
extensions[2]="bungcip.better-toml"
extensions[3]="ckolkman.vscode-postgres"
extensions[4]="cpylua.language-postcss"
extensions[5]="dbaeumer.vscode-eslint"
extensions[6]="eamodio.gitlens"
extensions[7]="EditorConfig.EditorConfig"
extensions[8]="hangxingliu.vscode-nginx-conf-hint"
extensions[9]="hex-ci.stylelint-plus"
extensions[10]="k--kato.intellij-idea-keybindings"
extensions[11]="ms-azuretools.vscode-cosmosdb"
extensions[12]="ms-azuretools.vscode-docker"
extensions[13]="ms-kubernetes-tools.vscode-kubernetes-tools"
extensions[14]="ms-vscode.azure-account"
extensions[15]="ms-vscode.csharp"
extensions[16]="ms-vscode.vscode-typescript-tslint-plugin"
extensions[17]="octref.vetur"
extensions[18]="PKief.material-icon-theme"
extensions[19]="prograhammer.tslint-vue"
extensions[20]="redhat.vscode-yaml"
extensions[21]="rust-lang.rust"
extensions[22]="shakram02.bash-beautify"
extensions[23]="Telerik.nativescript"
extensions[24]="vadimcn.vscode-lldb"

for extension in $extensions
do code --install-extension ${extension}
done
