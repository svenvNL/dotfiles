#!/bin/sh

. ./lib/os.sh
. ./lib/script.sh
get_os
get_workdir

echo -e "\n[ZSH] Installing"

if ! [ -f "$WORKDIR/install-${OS}.sh" ]; then
    echo "[ZSH] Unable to install";
    exit 1;
else
    . "$WORKDIR/install-${OS}.sh"
fi

sudo chsh --shell /bin/zsh

echo -e "\n[ZSH] Installing oh-my-zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

ln -fs "$WORKDIR/config/.zshrc" ~/.zshrc;
