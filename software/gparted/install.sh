#!/bin/sh

. ./lib/os.sh
. ./lib/script.sh
get_os
get_workdir

echo -e "\n[GParted] Installing"

if ! [ -f "$WORKDIR/install-${OS}.sh" ]; then
    echo "[GParted] Unable to install";
    exit 1;
else
    . "$WORKDIR/install-${OS}.sh"
fi
