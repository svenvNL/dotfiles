#!/bin/sh

. ./lib/os.sh
. ./lib/script.sh
get_os
get_workdir

echo -e "\n[PCManFm] Installing"

if ! [ -f "$WORKDIR/install-${OS}.sh" ]; then
    echo "[PCManFm] Unable to install";
    exit 1;
else
    . "$WORKDIR/install-${OS}.sh"
fi

ln -fs "$WORKDIR/config/pcmanfm.conf" ~/.config/pcmanfm/default/pcmanfm.conf;
