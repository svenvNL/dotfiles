#!/bin/sh

. ./lib/os.sh
. ./lib/script.sh
get_os
get_workdir

echo -e "\n[Alacritty] Installing"

if ! [ -f "$WORKDIR/install-${OS}.sh" ]; then
    echo "[Alacritty] Unable to install";
    exit 1;
else
    . "$WORKDIR/install-${OS}.sh"
fi

mkdir $HOME/.config/alacritty;
ln -fs "$WORKDIR/config/alacritty.yml" $HOME/.config/alacritty/alacritty.yml;
