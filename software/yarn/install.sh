#!/bin/sh

. ./lib/os.sh
. ./lib/script.sh
get_os
get_workdir

echo -e "\n[Yarn] Installing"

if ! [ -f "$WORKDIR/install-${OS}.sh" ]; then
    echo "[Yarn] Unable to install";
    exit 1;
else
    . "$WORKDIR/install-${OS}.sh"
fi

sudo yarn global add @vue/cli
