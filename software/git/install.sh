#!/bin/sh

. ./lib/script.sh
get_workdir

echo -e "\n[Git] Setting up"

read -p "Email": GIT_EMAIL
read -p "Name": GIT_NAME

GIT_CONFIG='[user]\n'
GIT_CONFIG+="email = ${GIT_EMAIL}\n"
GIT_CONFIG+="name = ${GIT_NAME}"
echo -e ${GIT_CONFIG} > ~/.gitconfig.local

echo -e "\n[Git] Linking"
ln -fs "$WORKDIR/.gitconfig" ~/.gitconfig
